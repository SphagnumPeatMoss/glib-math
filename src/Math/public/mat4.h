#pragma once
#include "forward.h"
#include "vec3.h"
#include "vec4.h"

namespace gas
{
	/***** Forward Declarations *****/
	struct mat3;
	struct quat;

	/***** Class Declaration *****/
	struct GOLDEN_API mat4
	{
		/***** Constants *****/
		static const int NUM_ELEMENTS = 16;
		static const int NUM_DIMENSIONS = 4;

		/***** Data *****/
		union
		{
			struct
			{
				real xRow[NUM_DIMENSIONS];
				real yRow[NUM_DIMENSIONS];
				real zRow[NUM_DIMENSIONS];
				real wRow[NUM_DIMENSIONS];
			};
			struct
			{
				vec4 xAxis;
				vec4 yAxis;
				vec4 zAxis;
				vec4 wAxis;
			};
			struct
			{
				vec3 right; real e14;
				vec3 up; real e24;
				vec3 forward; real e34;
				vec3 pos; real e44;
			};
			struct
			{
				vec4 axis[NUM_DIMENSIONS];
			};
			struct
			{
				real e11, e12, e13, e14;
				real e21, e22, e23, e24;
				real e31, e32, e33, e34;
				real e41, e42, e43, e44;
			};
			real e[NUM_ELEMENTS];
			real MxN[NUM_DIMENSIONS][NUM_DIMENSIONS];
		};

		/***** Public Interface *****/
		// Accessors
		real&			operator[](const int i);
		const real&		operator[](const int i) const;

		vec4			Column(const int i) const;
		vec4			ColumnX() const;
		vec4			ColumnY() const;
		vec4			ColumnZ() const;
		vec4			ColumnW() const;
		void			SetColumnX(const vec4& src);
		void			SetColumnY(const vec4& src);
		void			SetColumnZ(const vec4& src);
		void			SetColumnW(const vec4& src);

		// Comparisons
		bool			operator==(const mat4& rhs) const;
		bool			operator!=(const mat4& rhs) const;
		bool			IsZeroMatrix() const;

		// Arithmetic
		friend GOLDEN_API mat4	operator+(const mat4& m);
		mat4			operator+(const mat4& rhs) const;
		const mat4&		operator+=(const mat4& rhs);

		friend GOLDEN_API mat4 operator-(const mat4& m);
		mat4			operator-(const mat4& rhs) const;
		const mat4&		operator-=(const mat4& rhs);

		mat4			operator*(const mat4& rhs) const;
		const mat4&		operator*=(const mat4& rhs);
		vec4			operator*(const vec4& vec) const;
		mat4			operator*(const real s) const;
		const mat4&		operator*=(const real s);
		friend GOLDEN_API mat4 operator*(const real s, const mat4& m);

		mat4			operator/(const real s) const;
		const mat4&		operator/=(const real s);

		// Operations
		const mat4&			Transpose();
		mat4				Inverse() const;
		const mat4&			Invert();

		// Transformations
		const mat4&		TransformWorld(const mat4& transformMat);
		const mat4&		TransformWorldPreservePosition(const mat4& transformMat);
		const mat4&		TransformLocal(const mat4& transformMat);
		static mat4		CreateTranslationMatrix(const real x, const real y, const real z);
		static mat4		CreateTranslationMatrix(const vec3& src);
		static mat4		CreateScaleMatrix(const real x, const real y, const real z);
		static mat4		CreateScaleMatrix(const vec3& src);
		static mat4		CreateXRotationMatrix(const real radians);
		static mat4		CreateYRotationMatrix(const real radians);
		static mat4		CreateZRotationMatrix(const real radians);
		const mat4&		RotateAboutLocalX(const real xRadians);
		const mat4&		RotateAboutLocalY(const real xRadians);
		const mat4&		RotateAboutLocalZ(const real xRadians);
		const mat4&		OrbitWorldX(const real xRadians);		// does not preserve position
		const mat4&		OrbitWorldY(const real yRadians);		// does not preserve position
		const mat4&		OrbitWorldZ(const real zRadians);		// does not preserve position
		const mat4&		RotateAboutWorldX(const real xRadians);	// preserves position
		const mat4&		RotateAboutWorldY(const real yRadians);	// preserves position
		const mat4&		RotateAboutWorldZ(const real zRadians);	// preserves position
		const mat4&		SetScale(const vec3& src);
		const mat4&		SetScale(const real x, const real y, const real z);
		const mat4&		Scale(const vec3& src);
		const mat4&		Scale(const real x, const real y, const real z);
		const mat4&		ScalePreservePosition(const vec3& src);
		const mat4&		ScalePreservePosition(const real x, const real y, const real z);
		const mat4&		TranslateLocal(const vec3& direction, const real distance = 1);
		const mat4&		TranslateLocalForward(const real distance);
		const mat4&		TranslateLocalUp(const real distance);
		const mat4&		TranslateLocalRight(const real distance);
		const mat4&		TranslateGlobal(const vec3& direction, const real distance = 1);

		// Behaviors
		const mat4&		LookAt(const vec3& point, const vec3& up = vec3::WorldUp());
		const mat4&		TurnTo(const vec3& point, const real deltaRadians, const vec3& up = vec3::WorldUp());	// orthonormalizes the matrix (you will lose scale)
		const mat4&		OrthoNormalize(const vec3& worldUp = vec3::WorldUp());

		// Handedness
		const mat4&		InvertAxisX();
		const mat4&		InvertAxisY();
		const mat4&		InvertAxisZ();

		/***** Init *****/
		static mat4		ZeroMatrix();
		static mat4		IdentityMatrix();
		const mat4&		SetZeroMatrix();
		const mat4&		SetIdentityMatrix();

		mat4();
		mat4(const mat4& rhs) = default;
		mat4& operator=(const mat4& rhs) = default;
		~mat4() = default;

		mat4(const real m11, const real m12, const real m13, const real m14,
			const real m21, const real m22, const real m23, const real m24,
			const real m31, const real m32, const real m33, const real m34,
			const real m41, const real m42, const real m43, const real m44);
		mat4(const vec4& x, const vec4& y, const vec4& z, const vec4& w);
		explicit mat4(const quat& q);
		mat4(const quat& q, const vec3& translation);
		explicit mat4(const real* arr);
		mat4& operator=(const real* arr);

		mat3 To3x3() const;
		quat ToQuaternion() const;
		std::pair<quat, vec3> ToQuaternionPosition() const;
		vec3 ToAngles() const;
	};
}