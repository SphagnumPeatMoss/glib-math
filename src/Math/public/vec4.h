#pragma once
#include "forward.h"

namespace gas
{
	/***** Forward Declarations *****/
	struct vec2;
	struct vec3;
	struct mat4;

	/***** Class Declaration *****/
	struct GOLDEN_API vec4
	{
		/***** Constants *****/
		static const int NUM_ELEMENTS = 4;
		static vec4 WorldUp();

		/***** Data *****/
		union
		{
			struct { real x, y, z, w; };
			struct { real u, v; };
			struct { real r, g, b, a; };
			real e[NUM_ELEMENTS];
		};

		/***** Public Interface *****/
		// Accessors
		real&				operator[](const int i);
		const real&			operator[](const int i) const;
		vec3				AsVec3() const;

		// Comparisons
		bool				IsEqualTo(const vec4& rhs) const;
		bool				operator==(const vec4& rhs) const;
		bool				operator!=(const vec4& rhs) const;
		bool				IsZeroVector() const;

		// Arithmetic
		real				SumComponents() const;
		friend GOLDEN_API const vec4& operator+(const vec4& v);
		vec4				operator+(const vec4& rhs) const;
		const vec4&			operator+=(const vec4& rhs);

		friend GOLDEN_API vec4	operator-(const vec4& v);
		vec4				operator-(const vec4& rhs) const;
		const vec4&			operator-=(const vec4& rhs);

		vec4				operator*(const real scalar) const;
		const vec4&			operator*=(const real scalar);
		friend GOLDEN_API vec4	operator*(const real scalar, const vec4& v);

		vec4				operator/(const real divisor) const;
		const vec4&			operator/=(const real divisor);

		// Operations
		vec4				Normalized() const;
		const vec4&			SetNormalized();
		real				Magnitude() const;
		real				MagnitudeSquared() const;
		real				Dot(const vec4& rhs) const;
		real				ComponentOnto(const vec4& rhs) const;
		vec4				ProjectionOnto(const vec4& rhs) const;
		vec4				Cross(const vec4& rhs) const;
		vec4&				Transform(const mat4& m);

		/***** Init *****/
		static vec4			ZeroVector();

		vec4() = default;
		vec4(const vec4& rhs) = default;
		vec4& operator=(const vec4& rhs) = default;
		~vec4() = default;

		vec4(const vec2& rhs, const real _z, const real _w);
		vec4(const vec3& rhs, const real _w);
		vec4(const real _x, const real _y, const real _z, const real _w);
		explicit vec4(const real* arr);
		vec4& operator=(const real* arr);
	};
}