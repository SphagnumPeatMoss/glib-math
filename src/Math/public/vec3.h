#pragma once
#include "forward.h"

namespace gas
{
	/***** Forward Declarations *****/
	struct vec2;
	struct mat4;

	/***** Class Declaration *****/
	struct GOLDEN_API vec3
	{
		/***** Constants *****/
		static const int NUM_ELEMENTS = 3;
		static vec3 WorldUp();

		/***** Data *****/
		union
		{
			struct { real x, y, z; };
			struct { real u, v; };
			struct { real r, g, b; };
			real e[NUM_ELEMENTS];
		};

		/***** Public Interface *****/
		// Accessors
		real&			operator[](const int i);
		const real&		operator[](const int i) const;

		// Comparisons
		vec3			VectorTo(const vec3& toPoint) const;
		real			DistanceTo(const vec3& toPoint) const;
		vec3			DirectionTo(const vec3& toPoint) const;
		bool			IsOrthogonalTo(const vec3& rhs) const;
		bool			IsCollinearTo(const vec3& rhs) const;
		bool			IsEqualTo(const vec3& rhs) const;
		bool			operator==(const vec3& rhs) const;
		bool			operator!=(const vec3& rhs) const;
		bool			IsZeroVector() const;

		// Arithmetic
		real			SumComponents() const;
		friend GOLDEN_API const vec3& operator+(const vec3& v);
		vec3			operator+(const vec3& rhs) const;
		const vec3&		operator+=(const vec3& rhs);

		friend GOLDEN_API vec3 operator-(const vec3& v);
		vec3			operator-(const vec3& rhs) const;
		const vec3&		operator-=(const vec3& rhs);

		vec3			operator*(const real s) const;
		const vec3&		operator*=(const real s);
		friend GOLDEN_API vec3 operator*(const real s, const vec3& v);

		vec3			operator/(const real s) const;
		const vec3&		operator/=(const real s);

		// Operations
		static real		Offset(const vec3& point, const vec3& normal);
		static vec3		PointAlongNormal(const vec3& normal, const real offset);
		vec3			Normalized() const;
		const vec3&		SetNormalized();
		real			Magnitude() const;
		real			MagnitudeSquared() const;
		real			Dot(const vec3& rhs) const;
		real			ComponentOnto(const vec3& rhs) const;
		vec3			ProjectionOnto(const vec3& rhs) const;
		vec3			Cross(const vec3& rhs) const;
		vec3&			TransformNormal(const mat4& m);	// vectors representing direction are not affected by translation
		vec3&			TransformPoint(const mat4& m);	// vectors representing position are not affected by rotation

		// Angles
		AngleType		TypeOfAngleTo(const vec3& rhs);
		real			SmallestPositiveAngleBetween(const vec3& toDir) const;

		/***** Init *****/
		static vec3		ZeroVector();

		vec3() = default;
		vec3(const vec3& rhs) = default;
		vec3& operator=(const vec3& rhs) = default;
		~vec3() = default;

		vec3(const vec2& rhs, const real _z);
		vec3(const real _x, const real _y, const real _z);
		explicit vec3(const real* arr);
		vec3& operator=(const real* arr);
	};
}