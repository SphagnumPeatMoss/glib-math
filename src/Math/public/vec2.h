#pragma once
#include "forward.h"

namespace gas
{
	/***** Class Declaration *****/
	struct GOLDEN_API vec2
	{
		/***** Constants ******/
		static const int NUM_ELEMENTS = 2;
		static vec2 WorldUp();

		/**** Data *****/
		union
		{
			struct { real x, y; };
			struct { real u, v; };
			real e[NUM_ELEMENTS];
		};

		/***** Public Interface *****/
		// Accessors
		real&			operator[](const int i);
		const real&		operator[](const int i) const;

		// Comparisons
		vec2			VectorTo(const vec2& toPoint) const;
		real			DistanceTo(const vec2& toPoint) const;
		vec2			DirectionTo(const vec2& toPoint) const;
		bool			IsOrthogonalTo(const vec2& rhs) const;
		bool			IsCollinearTo(const vec2& rhs) const;
		bool			IsEqualTo(const vec2& rhs) const;
		bool			operator==(const vec2& rhs) const;
		bool			operator!=(const vec2& rhs) const;
		bool			IsZeroVector() const;

		// Arithmetic
		real			SumComponents() const;
		friend GOLDEN_API const vec2& operator+(const vec2& v);
		vec2			operator+(const vec2& rhs) const;
		const vec2&		operator+=(const vec2& rhs);

		friend GOLDEN_API vec2 operator-(const vec2& v);
		vec2			operator-(const vec2& rhs) const;
		const vec2&		operator-=(const vec2& rhs);

		vec2			operator*(const real s) const;
		const vec2&		operator*=(const real s);
		friend GOLDEN_API vec2 operator*(const real s, const vec2& v);

		vec2			operator/(const real s) const;
		const vec2&		operator/=(const real s);

		// Operations
		static real		Offset(const vec2& point, const vec2& normal);
		static vec2		PointAlongNormal(const vec2& normal, const real offset);
		vec2			Normalized() const;
		const vec2&		SetNormalized();
		real			Magnitude() const;
		real			MagnitudeSquared() const;
		real			Dot(const vec2& rhs) const;
		real			ComponenOnto(const vec2& rhs) const;
		vec2			ProjectionOnto(const vec2& rhs) const;
		real			Cross(const vec2& rhs) const;

		vec2			SkewCW() const;
		vec2			SkewCCW() const;

		// Angles
		real			AsRelativeAngle() const;	// -PI to PI, both are left
		real			AsAbsoluteAngle() const;	// 0 to 2PI, both are right
		real			AngleTo(const vec2& toDir) const;
		AngleType		TypeOfAngleTo(const vec2& rhs);

		/***** Init *****/
		static vec2		ZeroVector();

		vec2() = default;
		vec2(const vec2& rhs) = default;
		vec2& operator=(const vec2& rhs) = default;
		~vec2() = default;

		vec2(const real _x, const real _y);
		explicit vec2(const real* arr);
		vec2& operator=(const real* arr);
	};
}