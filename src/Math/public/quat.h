#pragma once
#include <utility>
#include "forward.h"
#include "vec3.h"

namespace gas
{
	/***** Forward Declarations *****/
	struct vec4;
	struct mat3;
	struct mat4;

	/***** Class Declarations *****/
	struct GOLDEN_API quat
	{
		/***** Constants *****/
		static const int NUM_ELEMENTS = 4;

		/***** Data *****/
		union
		{
			struct { real w, x, y, z; };
			struct { real w; vec3 v; };
			real e[NUM_ELEMENTS];
		};

		/***** Public Interface *****/
		// Accessors
		real&			operator[](const int i);
		const real&		operator[](const int i) const;
		mat3			ToRotationMatrix3D() const;
		mat4			ToRotationMatrix4D() const;

		// Comparisons
		quat			QuatTo(const quat& toDir) const;
		bool			operator==(const quat& rhs) const;
		bool			operator!=(const quat& rhs) const;

		// Arithmetic
		friend GOLDEN_API quat operator+(const quat& q);
		quat			operator+(const quat& rhs);
		const quat&		operator+=(const quat& rhs);

		friend GOLDEN_API quat operator-(const quat& q);
		quat			operator-(const quat& rhs);
		const quat&		operator-=(const quat& rhs);

		quat			operator*(const quat& rhs) const;	// opposite order of row-major matrices
		const quat&		operator*=(const quat& rhs);
		quat			operator*(const real s) const;
		const quat&		operator*=(const real s);
		quat			operator/(const real s) const;
		const quat&		operator/=(const real s);

		// Operations
		quat			Conjugate() const;
		const quat&		SetConjugate();
		quat			Inverse() const;
		const quat&		Invert();
		real			Dot(const quat& rhs) const;
		real			Magnitude() const;
		quat			SlerpTo(const quat& rhs, const real dt);

		/***** Init *****/
		static quat		Identity();

		quat();
		quat(const quat& rhs) = default;
		quat& operator=(const quat& rhs) = default;
		~quat() = default;

		quat(const real _w, const real _x, const real _y, const real _z);
		explicit quat(const mat3& rotationMatrix);
		explicit quat(const mat4& rotationMatrix);
		explicit quat(const real* arr);
	};
}
