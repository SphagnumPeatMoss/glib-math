#pragma once
#include "forward.h"
#include "vec2.h"
#include "vec3.h"

namespace gas
{
	/***** Forward Declarations *****/
	struct mat4;
	struct quat;

	/***** Class Declaration *****/
	struct GOLDEN_API mat3
	{
		/***** Constants *****/
		static const int NUM_ELEMENTS = 9;
		static const int NUM_DIMENSIONS = 3;

		/***** Data *****/
		union
		{
			struct
			{
				real xRow[NUM_DIMENSIONS];
				real yRow[NUM_DIMENSIONS];
				real zRow[NUM_DIMENSIONS];
			};
			struct
			{
				vec3 xAxis;
				vec3 yAxis;
				vec3 zAxis;
			};
			struct
			{
				vec3 right;
				vec3 up;
				vec3 forward;
			};
			struct
			{
				vec3 axis[NUM_DIMENSIONS];
			};
			struct
			{
				real e11, e12, e13;
				real e21, e22, e23;
				real e31, e32, e33;
			};
			real e[NUM_ELEMENTS];
			real MxN[NUM_DIMENSIONS][NUM_DIMENSIONS];
		};

		/***** Public Interface *****/
		// Accessors
		real&			operator[](const int i);
		const real&		operator[](const int i) const;
		mat4			ToRotation3D() const;

		vec3			Column(const int i) const;
		vec3			ColumnX() const;
		vec3			ColumnY() const;
		vec3			ColumnZ() const;
		void			SetColumnX(const vec3& src);
		void			SetColumnY(const vec3& src);
		void			SetColumnZ(const vec3& src);

		// Comparisons
		bool			operator==(const mat3& rhs) const;
		bool			operator!=(const mat3& rhs) const;
		bool			IsZeroMatrix() const;

		// Arithmetic
		friend GOLDEN_API mat3 operator+(const mat3& m);
		mat3			operator+(const mat3& rhs) const;
		const mat3&		operator+=(const mat3& rhs);

		friend GOLDEN_API mat3 operator-(const mat3& m);
		mat3			operator-(const mat3& rhs) const;
		const mat3&		operator-=(const mat3& rhs);

		mat3			operator*(const mat3& rhs) const;
		const mat3&		operator*=(const mat3& rhs);
		vec3			operator*(const vec3& vec) const;
		mat3			operator*(const real s) const;
		const mat3&		operator*=(const real s);
		friend GOLDEN_API mat3 operator*(const real s, const mat3& m);

		mat3			operator/(const real s) const;
		const mat3&		operator/=(const real s);

		// Operations
		const mat3&		Transpose();
		real			Determinant() const;
		mat3			Inverse() const;
		const mat3&		Invert();

		// Transformations
		const mat3&		TransformWorld(const mat3& transformMat);
		const mat3&		TransformLocal(const mat3& transformMat);
		static mat3		CreateXRotationMatrix(const real radians);
		static mat3		CreateYRotationMatrix(const real radians);
		static mat3		CreateZRotationMatrix(const real radians);
		const mat3&		RotateAboutLocalX(const real xRadians);
		const mat3&		RotateAboutLocalY(const real yRadians);
		const mat3&		RotateAboutLocalZ(const real zRadians);
		const mat3&		RotateAboutWorldX(const real xRadians);
		const mat3&		RotateAboutWorldY(const real yRadians);
		const mat3&		RotateAboutWorldZ(const real zRadians);

		// Behaviors
		const mat3&		OrthoNormalize(const vec3& up = vec3::WorldUp());

		// Handedness
		const mat3&		InvertAxisX();
		const mat3&		InvertAxisY();
		const mat3&		InvertAxisZ();

		/***** Init *****/
		static mat3		ZeroMatrix();
		static mat3		IdentityMatrix();
		const mat3&		SetZeroMatrix();
		const mat3&		SetIdentityMatrix();

		mat3();
		mat3(const mat3& rhs) = default;
		mat3& operator=(const mat3& rhs) = default;
		~mat3() = default;

		mat3(const real m11, const real m12, const real m13,
			const real m21, const real m22, const real m23,
			const real m31, const real m32, const real m33);
		mat3(const vec3& x, const vec3& y, const vec3& z);
		explicit mat3(const quat& q);
		explicit mat3(const real* arr);
		mat3& operator=(const real* arr);

		mat4 To4x4() const;
		quat ToQuaternion() const;
		vec3 ToAngles() const;
	};
}