#pragma once
#include "globals.h"

namespace gas
{
	/***** Constants *****/
	const real PI = (real)3.14159265;
	const real TWO_PI = PI * 2;
	const real HALF_PI = PI / 2;
	const real PI_OVER_6 = PI / 6;
	const real PI_OVER_4 = PI / 4;
	const real PI_OVER_3 = PI / 3;
	const real PI_OVER_2 = HALF_PI;

	enum class AngleType
	{
		Acute,	// <PI radians
		Obtuse, // >PI radians
		Right	// =PI radians
	};

	GOLDEN_API real	AsDegrees(const real radians);
	GOLDEN_API real	AsRadians(const real degrees);
	GOLDEN_API bool	IsZero(const real n);
	GOLDEN_API bool	IsEqual(const real a, const real b);
}