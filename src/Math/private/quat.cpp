#include "quat.h"
#include "mat3.h"
#include "mat4.h"

namespace gas
{
	/***** Public Interface *****/
	// Conversions
	real&			quat::operator[](const int i)
	{
		return e[i];
	}
	const real&		quat::operator[](const int i) const
	{
		return e[i];
	}
	mat3			quat::ToRotationMatrix3D() const
	{
		return ToRotationMatrix4D().To3x3();
	}
	mat4			quat::ToRotationMatrix4D() const
	{
// 		mat4 a(w, -z, y, x,
// 			z, w, -x, y,
// 			-y, x, w, z,
// 			-x, -y, -z, w
// 			);
// 		mat4 b(w, -z, y, -x,
// 			z, w, -x, -y,
// 			-y, x, w, -z,
// 			x, y, z, w
// 			);
// 
// 		return a * b;

		mat4 m		(1-2*y*y-2*z*z,		2*x*y+2*w*z,	2*x*z-2*w*y,	0,
					2*x*y-2*w*z,		1-2*x*x-2*z*z,	2*y*z+2*w*x,	0,
					2*x*z+2*w*y,		2*y*z-2*w*x,	1-2*x*x-2*y*y,	0,
					0,					0,				0,				1
		);
		return m;
	}

	// Comparisons
	quat			quat::QuatTo(const quat& toDir) const
	{
		// Get the quaternion from a to b:
		// da = b;
		// (da)aInv = b*aInv
		// d(a*aInv) = b*aInv
		// d[1 0] = b*aInv
		// d = b*aInv
		return toDir * Inverse();
	}
	bool			quat::operator==(const quat& rhs) const
	{
		for (int i = 0; i < quat::NUM_ELEMENTS; ++i)
		{
			if (!IsEqual(e[i], rhs[i]))
				return false;
		}
		return true;
	}
	bool			quat::operator!=(const quat& rhs) const
	{
		return !(*this == rhs);
	}

	// Arithmetic
	/*friend*/ quat operator+(const quat& q)
	{
		return q;
	}
	quat			quat::operator+(const quat& rhs)
	{
		return quat(w + rhs.w, x + rhs.x, y + rhs.y, z + rhs.z);
	}
	const quat&		quat::operator+=(const quat& rhs)
	{
		*this = (*this) + rhs;
		return *this;
	}

	/*friend*/ quat operator-(const quat& q)
	{
		return quat(-q.w, -q.x, -q.y, -q.z);
	}
	quat			quat::operator-(const quat& rhs)
	{
		return (*this) + (-rhs);
	}
	const quat&		quat::operator-=(const quat& rhs)
	{
		*this = *this - rhs;
		return *this;
	}

	quat			quat::operator*(const quat& rhs) const
	{
		const quat& a = *this;
		const quat& b = rhs;
		const vec3 av(a.x, a.y, a.z);
		const vec3 bv(b.x, b.y, b.z);
		quat result;

		/********** Compute the w **********/
		// 1. Multiply the w's of each quat
		real wProduct = a.w * b.w;

		// 2. Take the dot product of each quat's vector
		real dotProduct = av.Dot(bv);

		// 3. Subtract the dot product from the w product
		result.w = wProduct - dotProduct;

		/********** Compute the v **********/
		// 1. Scale the second quat's v by the first quat's w
		vec3 v1 = a.w * bv;

		// 2. Scale the first quat's v by the second quat's w
		vec3 v2 = b.w * av;

		// 3. Cross the two vectors from each quat
		vec3 v3 = av.Cross(bv);

		// 4. Sum the results of #1,#2,#3
		vec3 sum = v1 + v2 + v3;
		result.x = sum.x;
		result.y = sum.y;
		result.z = sum.z;

		return result;
	}
	const quat&		quat::operator*=(const quat& rhs)
	{
		*this = (*this) * rhs;
		return *this;
	}
	quat			quat::operator*(const real s) const
	{
		quat q(*this);
		q.x *= s;
		q.y *= s;
		q.z *= s;
		q.w *= s;
		return q;
	}
	const quat&		quat::operator*=(const real s)
	{
		(*this) = (*this) * s;
		return *this;
	}
	quat			quat::operator/(const real s) const
	{
		quat q(*this);
		q.x /= s;
		q.y /= s;
		q.z /= s;
		q.w /= s;
		return q;
	}
	const quat&		quat::operator/=(const real s)
	{
		(*this) = (*this) / s;
		return *this;
	}

	// Operations
	quat			quat::Conjugate() const
	{
		// Negate the vector portion
		quat q;
		q.w = w;
		q.x = -q.x;
		q.y = -q.y;
		q.z = -q.z;
		return q;
	}
	const quat&		quat::SetConjugate()
	{
		*this = Conjugate();
		return *this;
	}
	quat			quat::Inverse() const
	{
		// Inverse = conjugate / magnitude
		quat ret = *this;
		ret.Invert();
		return ret;
	}
	const quat&		quat::Invert()
	{
		const quat conj = Conjugate();
		*this = quat(conj / Magnitude());
		return *this;
	}
	real			quat::Dot(const quat& rhs) const
	{
		const quat& b = rhs;
		return vec4(w, x, y, z).Dot(vec4(b.w, b.x, b.y, b.z));
	}
	real			quat::Magnitude() const
	{
		return vec4(w, x, y, z).Magnitude();
	}
	quat			quat::SlerpTo(const quat& rhs, const real dt)
	{
		const quat& a = *this;
		quat b = rhs;
		quat result;

		// Compute the cosine of the angle between the quats
		float cosOmega = a.w*b.w + a.x*b.x + a.y*b.y + a.z*b.z;

		// if negative dot, negate one of the input quats to take the shorter 4D arc
		if (cosOmega < 0)
		{
			b.w = -b.w;
			b.x = -b.x;
			b.y = -b.y;
			b.z = -b.z;
			cosOmega = -cosOmega;
		}

		// check if they are very close together, to preotect against divide by zero
		float k0, k1;
		if (cosOmega > real(0.9999))
		{
			// very close - just use linear interpolation
			k0 = 1 - dt;
			k1 = dt;
		}
		else
		{
			// compute the sin of the angle using trig identity sin^2(omega) + cos^2(omega) = 1
			real sinOmega = sqrt(1 - cosOmega * cosOmega);

			// Compute the angle from its sin and cos
			real omega = atan2(sinOmega, cosOmega);

			// compute inverse of denominator, so we only have to divide once
			real oneOverSinOmega = 1 / sinOmega;

			// compute interpolation parameters
			k0 = sin((1.0f - dt) * omega) * oneOverSinOmega;
			k1 = sin(dt * omega) * oneOverSinOmega;
		}

		// Interpolate
		result.w = a.w*k0 + b.w*k1;
		result.x = a.x*k0 + b.x*k1;
		result.y = a.y*k0 + b.y*k1;
		result.z = a.z*k0 + b.z*k1;

		return result;
	}

	/***** Init *****/
	/*static*/ quat		quat::Identity()
	{
		static const quat identity;
		return identity;
	}

	quat::quat() :
		quat(1, 0, 0, 0)
	{

	}
	quat::quat(const real _w, const real _x, const real _y, const real _z) :
		w(_w), x(_x), y(_y), z(_z)
	{

	}
	/*explicit*/ quat::quat(const mat3& rotationMatrix)
	{
		*this = rotationMatrix.ToQuaternion();
	}
	/*explicit*/ quat::quat(const mat4& rotationMatrix)
	{
		*this = rotationMatrix.ToQuaternion();
	}
	/*explicit*/ quat::quat(const real* arr) :
		quat(arr[0], arr[1], arr[2], arr[3])
	{

	}
}