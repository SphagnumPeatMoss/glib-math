#include "vec3.h"
#include "vec2.h"
#include "mat4.h"
#include "tvec.hpp"

namespace gas
{
	/***** Aliases *****/
	typedef tvec<vec3, mat4> Algorithm;
	typedef tvec<vec4, mat4> Vec4Algorithm;

	/***** Public Interface *****/
	/*static*/ vec3 vec3::WorldUp()
	{
		static const vec3 up(0, 1, 0);
		return up;
	}

	// Accessors
	real&			vec3::operator[](const int i)
	{
		return e[i];
	}
	const real&		vec3::operator[](const int i) const
	{
		return e[i];
	}

	// Comparisons
	vec3			vec3::VectorTo(const vec3& rhs) const
	{
		return Algorithm::VectorTo(*this, rhs);
	}
	real			vec3::DistanceTo(const vec3& rhs) const
	{
		return Algorithm::DistanceBetween(*this, rhs);
	}
	vec3			vec3::DirectionTo(const vec3& toPoint) const
	{
		return Algorithm::DirectionTo(*this, toPoint);
	}
	bool			vec3::IsOrthogonalTo(const vec3& rhs) const
	{
		return Algorithm::IsOrthogonal(*this, rhs);
	}
	bool			vec3::IsCollinearTo(const vec3& rhs) const
	{
		return Algorithm::IsCollinear(*this, rhs);
	}
	bool			vec3::IsEqualTo(const vec3& rhs) const
	{
		return Algorithm::IsEqualVector(*this, rhs);
	}
	bool			vec3::operator==(const vec3& rhs) const
	{
		return Algorithm::IsEqualVector(*this, rhs);
	}
	bool			vec3::operator!=(const vec3& rhs) const
	{
		return !Algorithm::IsEqualVector(*this, rhs);
	}
	bool			vec3::IsZeroVector() const
	{
		return Algorithm::IsZeroVector(*this);
	}

	// Arithmetic
	real			vec3::SumComponents() const
	{
		return Algorithm::SumComponents(*this);
	}
	/*friend*/ const vec3& operator+(const vec3& v)
	{
		return v;
	}
	vec3			vec3::operator+(const vec3& rhs) const
	{
		return Algorithm::Add(*this, rhs);
	}
	const vec3&		vec3::operator+=(const vec3& rhs)
	{
		*this = *this + rhs;
		return *this;
	}

	/*friend*/ vec3 operator-(const vec3& v)
	{
		return Algorithm::Negative(v);
	}
	vec3			vec3::operator-(const vec3& rhs) const
	{
		return Algorithm::Subtract(*this, rhs);
	}
	const vec3&		vec3::operator-=(const vec3& rhs)
	{
		*this = *this - rhs;
		return *this;
	}

	vec3			vec3::operator*(const real s) const
	{
		return Algorithm::Multiply(*this, s);
	}
	const vec3&		vec3::operator*=(const real s)
	{
		*this = *this * s;
		return *this;
	}
	/*friend*/ vec3 operator*(const real s, const vec3& v)
	{
		return v * s;
	}

	vec3			vec3::operator/(const real s) const
	{
		return Algorithm::Divide(*this, s);
	}
	const vec3&		vec3::operator/=(const real s)
	{
		*this = *this / s;
		return *this;
	}

	// Operations
	/*static*/ real	vec3::Offset(const vec3& point, const vec3& normal)
	{
		return Algorithm::Offset(point, normal);
	}
	/*static*/ vec3	vec3::PointAlongNormal(const vec3& normal, const real offset)
	{
		return Algorithm::PointAlongNormal(normal, offset);
	}
	vec3			vec3::Normalized() const
	{
		return Algorithm::Normalized(*this);
	}
	const vec3&		vec3::SetNormalized()
	{
		*this = Normalized();
		return *this;
	}
	real			vec3::Magnitude() const
	{
		return Algorithm::Magnitude(*this);
	}
	real			vec3::MagnitudeSquared() const
	{
		return Algorithm::MagnitudeSquared(*this);
	}
	real			vec3::Dot(const vec3& rhs) const
	{
		return Algorithm::Dot(*this, rhs);
	}
	real			vec3::ComponentOnto(const vec3& rhs) const
	{
		return Algorithm::ComponentOnto(*this, rhs);
	}
	vec3			vec3::ProjectionOnto(const vec3& rhs) const
	{
		return Algorithm::ProjectionOnto(*this, rhs);
	}
	vec3			vec3::Cross(const vec3& rhs) const
	{
		vec3 crossed;
		crossed.x = (y * rhs.z - rhs.y * z);
		crossed.y = (z * rhs.x - rhs.z * x);
		crossed.z = (x * rhs.y - rhs.x * y);
		return crossed;
	}
	vec3&			vec3::TransformNormal(const mat4& m)
	{
		vec4 v(*this, 0);
		v = m * v;
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}
	vec3&			vec3::TransformPoint(const mat4& m)
	{
		vec4 v(*this, 1);
		v = m * v;
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}

	// Angles
	AngleType		vec3::TypeOfAngleTo(const vec3& rhs)
	{
		return Algorithm::TypeOfAngleBetween(*this, rhs);
	}

	real vec3::SmallestPositiveAngleBetween(const vec3& toDir) const
	{
		const real dot = Normalized().Dot(toDir.Normalized());
		return acos(dot);
	}


	/***** Init *****/
	/*static*/ vec3		vec3::ZeroVector()
	{
		return vec3(0, 0, 0);
	}

	vec3::vec3(const vec2& rhs, const real _z) :
		vec3(rhs.x, rhs.y, _z)
	{

	}
	vec3::vec3(const real _x, const real _y, const real _z) :
		x(_x), y(_y), z(_z)
	{

	}
	/*explicit*/ vec3::vec3(const real* arr) :
		vec3(arr[0], arr[1], arr[2])
	{

	}
	vec3& vec3::operator=(const real* arr)
	{
		x = arr[0];
		y = arr[1];
		z = arr[2];
		return *this;
	}
}