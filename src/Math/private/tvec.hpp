#pragma once
#include "forward.h"
#include <cmath>

namespace gas
{
	template <typename vec, typename mat> struct tvec
	{
	public:
		// Comparisons
		static vec		VectorTo(const vec& fromPoint, const vec& toPoint)
		{
			return toPoint - fromPoint;
		}
		static real		DistanceBetween(const vec& fromPoint, const vec& toPoint)
		{
			return Magnitude(toPoint - fromPoint);
		}
		static vec		DirectionTo(const vec& fromPoint, const vec& toPoint)
		{
			return Normalized(VectorTo(fromPoint, toPoint));
		}
		static bool		IsOrthogonal(const vec& a, const vec& b)
		{
			return !IsZeroVector(a) && !IsZeroVector(b) && IsZero(Dot(a, b));
		}
		static bool		IsCollinear(const vec& a, const vec& b)
		{
			return !IsZeroVector(a) && !IsZeroVector(b) && Normalized(a) == Normalized(b);
		}
		static bool		IsEqualVector(const vec& a, const vec& b)
		{
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				if (!IsEqual(a[i], b[i]))
					return false;
			}
			return true;
		}
		static bool		IsZeroVector(const vec& v)
		{
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				if (!IsZero(v[i]))
					return false;
			}
			return true;
		}

		// Arithmetic
		static real		SumComponents(const vec& v)
		{
			real sum = 0;
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				sum += v[i];
			}
			return sum;
		}
		static vec		Negative(const vec& v)
		{
			vec result;
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				result[i] = -v[i];
			}
			return result;
		}
		static vec		Add(const vec& a, const vec& b)
		{
			vec sum;
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				sum[i] = a[i] + b[i];
			}
			return sum;
		}
		static vec		Subtract(const vec& a, const vec& b)
		{
			vec diff;
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				diff[i] = a[i] - b[i];
			}
			return diff;
		}
		static vec		Multiply(const vec& v, const real s)
		{
			vec product;
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				product[i] = v[i] * s;
			}
			return product;
		}
		static vec		Divide(const vec& v, const real s)
		{
			assert(!IsZero(s) && "Attempt to divide by zero.");
			vec quotient;
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				quotient[i] = v[i] / s;
			}
			return quotient;
		}

		// Operations
		static vec		PointAlongNormal(const vec& normal, const real offset)
		{
			return Multiply(normal, offset);
		}
		static real		Offset(const vec& point, const vec& normal)
		{
			return ComponentOnto(point, normal);
		}
		static vec		Normalized(const vec& v)
		{
			return v / Magnitude(v);
		}
		static real		Magnitude(const vec& v)
		{
			return sqrt(MagnitudeSquared(v));
		}
		static real		MagnitudeSquared(const vec& v)
		{
			return Dot(v, v);
		}
		static real		Dot(const vec& a, const vec& b)
		{
			vec products;
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				products[i] = a[i] * b[i];
			}
			return SumComponents(products);
		}
		static real		ComponentOnto(const vec& a, const vec& b)
		{
			return Dot(a, Normalized(b));
		}
		static vec		ProjectionOnto(const vec& a, const vec& b)
		{
			return (ComponentOnto(a, b) * Normalized(b));
		}

		// Angles
		static AngleType TypeOfAngleBetween(const vec& a, const vec& b)
		{
			real dotProduct = Dot(a, b);

			if (dotProduct < -EPSILON)
				return AngleType::Obtuse;
			else if (dotProduct > EPSILON)
				return AngleType::Acute;
			else
				return AngleType::Right;
		}

	};
}