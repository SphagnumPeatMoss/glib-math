#include "vec2.h"
#include "tvec.hpp"
#include "mat3.h"

namespace gas
{
	/***** Aliases *****/
	typedef tvec<vec2, mat3> Algorithm;

	/***** Public Interface *****/
	/*static*/			vec2 vec2::WorldUp()
	{
		static const vec2 up(0, 1);
		return up;
	}

	// Accessors
	real&				vec2::operator[](const int i)
	{
		return e[i];
	}
	const real&			vec2::operator[](const int i) const
	{
		return e[i];
	}

	// Comparisons
	vec2				vec2::VectorTo(const vec2& toPoint) const
	{
		return Algorithm::VectorTo(*this, toPoint);
	}
	real				vec2::DistanceTo(const vec2& toPoint) const
	{
		return Algorithm::DistanceBetween(*this, toPoint);
	}
	vec2				vec2::DirectionTo(const vec2& toPoint) const
	{
		return Algorithm::DirectionTo(*this, toPoint);
	}
	bool				vec2::IsOrthogonalTo(const vec2& rhs) const
	{
		return Algorithm::IsOrthogonal(*this, rhs);
	}
	bool				vec2::IsCollinearTo(const vec2& rhs) const
	{
		return Algorithm::IsCollinear(*this, rhs);
	}
	bool				vec2::IsEqualTo(const vec2& rhs) const
	{
		return Algorithm::IsEqualVector(*this, rhs);
	}
	bool				vec2::operator==(const vec2& rhs) const
	{
		return Algorithm::IsEqualVector(*this, rhs);
	}
	bool				vec2::operator!=(const vec2& rhs) const
	{
		return !Algorithm::IsEqualVector(*this, rhs);
	}
	bool				vec2::IsZeroVector() const
	{
		return Algorithm::IsZeroVector(*this);
	}

	// Arithmetic
	real				vec2::SumComponents() const
	{
		return Algorithm::SumComponents(*this);
	}
	/*friend*/ const vec2&	operator+(const vec2& v)
	{
		return v;
	}
	vec2				vec2::operator+(const vec2& rhs) const
	{
		return Algorithm::Add(*this, rhs);
	}
	const vec2&			vec2::operator+=(const vec2& rhs)
	{
		*this = *this + rhs;
		return *this;
	}

	/*friend*/ vec2		operator-(const vec2& v)
	{
		return Algorithm::Negative(v);
	}
	vec2				vec2::operator-(const vec2& rhs) const
	{
		return Algorithm::Subtract(*this, rhs);
	}
	const vec2&			vec2::operator-=(const vec2& rhs)
	{
		*this = *this - rhs;
		return *this;
	}

	vec2				vec2::operator*(const real s) const
	{
		return Algorithm::Multiply(*this, s);
	}
	const vec2&			vec2::operator*=(const real s)
	{
		*this = *this * s;
		return *this;
	}
	/*friend*/ vec2		operator*(const real s, const vec2& v)
	{
		return v * s;
	}

	vec2				vec2::operator/(const real s) const
	{
		return Algorithm::Divide(*this, s);
	}
	const vec2&			vec2::operator/=(const real s)
	{
		*this = *this / s;
		return *this;
	}

	// Operations
	/*static*/ real		vec2::Offset(const vec2& point, const vec2& normal)
	{
		return Algorithm::Offset(point, normal);
	}
	/*static*/ vec2		vec2::PointAlongNormal(const vec2& normal, const real offset)
	{
		return Algorithm::PointAlongNormal(normal, offset);
	}
	vec2				vec2::Normalized() const
	{
		return Algorithm::Normalized(*this);
	}
	const vec2&			vec2::SetNormalized()
	{
		*this = Normalized();
		return *this;
	}
	real				vec2::Magnitude() const
	{
		return Algorithm::Magnitude(*this);
	}
	real				vec2::MagnitudeSquared() const
	{
		return Algorithm::MagnitudeSquared(*this);
	}
	real				vec2::Dot(const vec2& rhs) const
	{
		return Algorithm::Dot(*this, rhs);
	}
	real				vec2::ComponenOnto(const vec2& rhs) const
	{
		return Algorithm::ComponentOnto(*this, rhs);
	}
	vec2				vec2::ProjectionOnto(const vec2& rhs) const
	{
		return Algorithm::ProjectionOnto(*this, rhs);
	}
	real				vec2::Cross(const vec2& rhs) const
	{
		return (x * rhs.y) - (y * rhs.x);
	}

	vec2				vec2::SkewCW() const
	{
		return vec2(y, -x);
	}
	vec2				vec2::SkewCCW() const
	{
		return vec2(-y, x);
	}

	// Angles
	real				vec2::AsRelativeAngle() const
	{
		return atan2(y, x);
	}
	real				vec2::AsAbsoluteAngle() const
	{
		real a = AsRelativeAngle();
		if (a < 0)
			a += (2 * PI);
		return a;
	}
	real				vec2::AngleTo(const vec2& toDir) const
	{
		real a = AsAbsoluteAngle();
		real b = toDir.AsAbsoluteAngle();
		return atan2(sin(b - a), cos(b - a));
	}
	AngleType			vec2::TypeOfAngleTo(const vec2& rhs)
	{
		return Algorithm::TypeOfAngleBetween(*this, rhs);
	}

	/***** Init *****/
	/*static*/ vec2			vec2::ZeroVector()
	{
		return vec2(0, 0);
	}

	vec2::vec2(const real _x, const real _y)
		: x(_x), y(_y)
	{

	}
	/*explicit*/ vec2::vec2(const real* arr)
		: vec2(arr[0], arr[1])
	{

	}
	vec2& vec2::operator=(const real* arr)
	{
		x = arr[0];
		y = arr[1];
		return *this;
	}
}