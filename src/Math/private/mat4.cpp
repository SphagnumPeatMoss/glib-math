#include "mat4.h"
#include "mat3.h"
#include "quat.h"
#include "vec3.h"
#include "vec4.h"
#include "tmat.hpp"

namespace gas
{
	typedef tmat<mat4, vec4> Algorithm;

	/***** Public Interface *****/
	// Accessors
	real&			mat4::operator[](const int i)
	{
		return e[i];
	}
	const real&		mat4::operator[](const int i) const
	{
		return e[i];
	}

	vec4			mat4::Column(const int i) const
	{
		return vec4(MxN[0][i], MxN[1][i], MxN[2][i], MxN[3][i]);
	}
	vec4			mat4::ColumnX() const
	{
		return vec4(e11, e21, e31, e41);
	}
	vec4			mat4::ColumnY() const
	{
		return vec4(e12, e22, e32, e42);
	}
	vec4			mat4::ColumnZ() const
	{
		return vec4(e13, e23, e33, e43);
	}
	vec4			mat4::ColumnW() const
	{
		return vec4(e14, e24, e34, e44);
	}
	void			mat4::SetColumnX(const vec4& src)
	{
		e11 = src.x;
		e21 = src.y;
		e31 = src.z;
		e41 = src.w;
	}
	void			mat4::SetColumnY(const vec4& src)
	{
		e12 = src.x;
		e22 = src.y;
		e32 = src.z;
		e42 = src.w;
	}
	void			mat4::SetColumnZ(const vec4& src)
	{
		e13 = src.x;
		e23 = src.y;
		e33 = src.z;
		e43 = src.w;
	}
	void			mat4::SetColumnW(const vec4& src)
	{
		e14 = src.x;
		e24 = src.y;
		e34 = src.z;
		e44 = src.w;
	}

	// Comparisons
	bool			mat4::operator==(const mat4& rhs) const
	{
		return Algorithm::IsMatrixEqual(*this, rhs);
	}
	bool			mat4::operator!=(const mat4& rhs) const
	{
		return !(*this == rhs);
	}
	bool			mat4::IsZeroMatrix() const
	{
		return Algorithm::IsZeroMatrix(*this);
	}

	// Arithmetic
	/*friend*/ mat4	operator+(const mat4& m)
	{
		return m;
	}
	mat4			mat4::operator+(const mat4& rhs) const
	{
		return Algorithm::Add(*this, rhs);
	}
	const mat4&		mat4::operator+=(const mat4& rhs)
	{
		*this = *this + rhs;
		return *this;
	}

	/*friend*/ mat4 operator-(const mat4& m)
	{
		return Algorithm::Negative(m);
	}
	mat4			mat4::operator-(const mat4& rhs) const
	{
		return Algorithm::Subtract(*this, rhs);
	}
	const mat4&		mat4::operator-=(const mat4& rhs)
	{
		*this = *this - rhs;
		return *this;
	}

	mat4			mat4::operator*(const mat4& rhs) const
	{
		return Algorithm::Multiply(*this, rhs);
	}
	const mat4&		mat4::operator*=(const mat4& rhs)
	{
		*this = *this * rhs;
		return *this;
	}
	vec4			mat4::operator*(const vec4& vec) const
	{
		return Algorithm::Transform(*this, vec);
	}
	mat4			mat4::operator*(const real s) const
	{
		return Algorithm::Multiply(*this, s);
	}
	const mat4&		mat4::operator*=(const real s)
	{
		*this = *this * s;
		return *this;
	}
	/*friend*/ mat4 operator*(const real s, const mat4& m)
	{
		return m * s;
	}

	mat4			mat4::operator/(const real s) const
	{
		return Algorithm::Divide(*this, s);
	}
	const mat4&		mat4::operator/=(const real s)
	{
		*this = *this / s;
		return *this;
	}

	// Operations
	const mat4&			mat4::Transpose()
	{
		Algorithm::Transpose(*this);
		return *this;
	}
	mat4				mat4::Inverse() const
	{
		mat4 result;

		// Transpose the inner 3x3
		result.e11 = e11;	result.e21 = e12;	result.e31 = e13;
		result.e12 = e21;	result.e22 = e22;	result.e32 = e23;
		result.e13 = e31;	result.e23 = e32;	result.e33 = e33;

		// Retain the w-col
		result.SetColumnW(ColumnW());

		// Set the w-axis to negated dot product of w-axis and each row
		vec4 x, y, z, w;
		x = xAxis;
		y = yAxis;
		z = zAxis;
		w = wAxis;

		w.x = -w.Dot(x);
		w.y = -w.Dot(y);
		w.z = -w.Dot(z);

		result.wAxis = w;

		return result;
	}
	const mat4&			mat4::Invert()
	{
		*this = Inverse();
		return *this;
	}

	// Transformations
	const mat4&			mat4::TransformWorld(const mat4& transformMat)
	{
		*this = Algorithm::TransformWorld(*this, transformMat);
		return *this;
	}
	const mat4&			mat4::TransformWorldPreservePosition(const mat4& transformMat)
	{
		// Save old position
		vec3 translation = pos;

		// Zero out position on the matrix
		pos.x = pos.y = pos.z = 0;

		// Rotate the matrix
		TransformWorld(transformMat);

		// Reinstate original position
		pos = translation;

		return *this;
	}
	const mat4&			mat4::TransformLocal(const mat4& transformMat)
	{
		*this = Algorithm::TransformLocal(*this, transformMat);
		return *this;
	}
	/*static*/ mat4		mat4::CreateTranslationMatrix(const real x, const real y, const real z)
	{
		return mat4(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			x, y, z, 1
			);
	}
	/*static*/ mat4		mat4::CreateTranslationMatrix(const vec3& src)
	{
		return CreateTranslationMatrix(src.x, src.y, src.z);
	}
	/*static*/ mat4		mat4::CreateScaleMatrix(const real x, const real y, const real z)
	{
		return mat4(
			x, 0, 0, 0,
			0, y, 0, 0,
			0, 0, z, 0,
			0, 0, 0, 1
			);
	}
	/*static*/ mat4		mat4::CreateScaleMatrix(const vec3& src)
	{
		return CreateScaleMatrix(src.x, src.y, src.z);
	}
	/*static*/ mat4		mat4::CreateXRotationMatrix(const real radians)
	{
		return mat4(
			1, 0, 0, 0,
			0, cos(radians), sin(radians), 0,
			0, -(sin(radians)), cos(radians), 0,
			0, 0, 0, 1
			);
	}
	/*static*/ mat4		mat4::CreateYRotationMatrix(const real radians)
	{
		return mat4(
			cos(radians), 0, -(sin(radians)), 0,
			0, 1, 0, 0,
			sin(radians), 0, cos(radians), 0,
			0, 0, 0, 1
			);
	}
	/*static*/ mat4		mat4::CreateZRotationMatrix(const real radians)
	{
		return mat4(
			cos(radians), sin(radians), 0, 0,
			-(sin(radians)), cos(radians), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
			);
	}
	const mat4&			mat4::RotateAboutLocalX(const real xRadians)
	{
		mat4 rotMatrix = CreateXRotationMatrix(xRadians);
		return TransformLocal(rotMatrix);
	}
	const mat4&			mat4::RotateAboutLocalY(const real xRadians)
	{
		mat4 rotMatrix = CreateYRotationMatrix(xRadians);
		return TransformLocal(rotMatrix);
	}
	const mat4&			mat4::RotateAboutLocalZ(const real xRadians)
	{
		mat4 rotMatrix = CreateZRotationMatrix(xRadians);
		return TransformLocal(rotMatrix);
	}
	const mat4&			mat4::OrbitWorldX(const real xRadians)
	{
		mat4 rotMatrix = CreateXRotationMatrix(xRadians);
		return TransformWorld(rotMatrix);
	}
	const mat4&			mat4::OrbitWorldY(const real yRadians)
	{
		mat4 rotMatrix = CreateYRotationMatrix(yRadians);
		return TransformWorld(rotMatrix);
	}
	const mat4&			mat4::OrbitWorldZ(const real zRadians)
	{
		mat4 rotMatrix = CreateZRotationMatrix(zRadians);
		return TransformWorld(rotMatrix);
	}
	const mat4&			mat4::RotateAboutWorldX(const real xRadians)
	{
		mat4 rotMatrix = CreateXRotationMatrix(xRadians);
		return TransformWorldPreservePosition(rotMatrix);
	}
	const mat4&			mat4::RotateAboutWorldY(const real yRadians)
	{
		mat4 rotMatrix = CreateYRotationMatrix(yRadians);
		return TransformWorldPreservePosition(rotMatrix);
	}
	const mat4&			mat4::RotateAboutWorldZ(const real zRadians)
	{
		mat4 rotMatrix = CreateZRotationMatrix(zRadians);
		return TransformWorldPreservePosition(rotMatrix);
	}
	const mat4&			mat4::SetScale(const vec3& src)
	{
		return SetScale(src.x, src.y, src.z);
	}
	const mat4&			mat4::SetScale(const real x, const real y, const real z)
	{
		OrthoNormalize();
		xAxis *= x;
		yAxis *= y;
		zAxis *= z;
		return *this;
	}
	const mat4&			mat4::Scale(const vec3& src)
	{
		return Scale(src.x, src.y, src.z);
	}
	const mat4&			mat4::Scale(const real x, const real y, const real z)
	{
		mat4 scaleMat = CreateScaleMatrix(x, y, z);
		return TransformLocal(scaleMat);
	}
	const mat4&			mat4::ScalePreservePosition(const vec3& src)
	{
		return ScalePreservePosition(src.x, src.y, src.z);
	}
	const mat4&			mat4::ScalePreservePosition(const real x, const real y, const real z)
	{
		// Save the old position
		vec3 translation = pos;

		// Zero out position on the matrix;
		pos.x = pos.y = pos.z = 0;

		// Scale the matrix
		mat4 scaleMat = CreateScaleMatrix(x, y, z);
		TransformLocal(scaleMat);

		// Reinstate original position
		pos = translation;

		return *this;
	}
	const mat4&			mat4::TranslateLocal(const vec3& direction, const real distance /*= 1*/)
	{
		TranslateLocalForward(direction.z * distance);
		TranslateLocalUp(direction.y * distance);
		TranslateLocalRight(direction.x * distance);
		return *this;
	}
	const mat4&			mat4::TranslateLocalForward(const real distance)
	{
		wAxis += zAxis * distance;
		return *this;
	}
	const mat4&			mat4::TranslateLocalUp(const real distance)
	{
		wAxis += yAxis * distance;
		return *this;
	}
	const mat4&			mat4::TranslateLocalRight(const real distance)
	{
		wAxis += xAxis * distance;
		return *this;
	}
	const mat4&			mat4::TranslateGlobal(const vec3& direction, const real distance /*= 1*/)
	{
		wAxis += vec4(direction * distance, 0);
		return *this;
	}

	// Behaviors
	const mat4&			mat4::LookAt(const vec3& point, const vec3& myUp /*= vec3::WorldUp()*/)
	{
		// Get our position vectors
		vec3 myPos = vec3(wAxis.x, wAxis.y, wAxis.z);

		// Get a vector to the target
		vec3 toTarget = point - myPos;
		toTarget.SetNormalized();

		// Make it my new look vector
		zAxis = vec4(toTarget, 0);

		// Re-orient my matrix about the new z
		OrthoNormalize(myUp);

		return *this;
	}
	const mat4&			mat4::TurnTo(const vec3& point, const real deltaRadians, const vec3& myUp /*= vec3::WorldUp()*/)
	{
		// Get the positions and a vector to the target
		vec3 myPos = vec3(wAxis.x, wAxis.y, wAxis.z);
		vec3 toTarget = point - myPos;

		const real a = forward.SmallestPositiveAngleBetween(toTarget);
		if (a > deltaRadians)
		{
			// Calculate the y-axis rotation
			vec3 xVector = vec3(xAxis.x, xAxis.y, xAxis.z);
			real dpRight = toTarget.Dot(xVector);
			if (IsZero(dpRight))
			{
				if (toTarget != forward)
				{
					dpRight = 1;
				}
			}
			real yRot = dpRight > 0 ? deltaRadians : -deltaRadians;

			// Calculate the x-axis rotation
			vec3 yVector = vec3(yAxis.x, yAxis.y, yAxis.z);
			real dpUp = (-toTarget).Dot(yVector);
			if (IsZero(dpUp))
			{
				if (toTarget != forward)
				{
					dpUp = 1;
				}
			}
			real xRot = dpUp > 0 ? deltaRadians : -deltaRadians;

			// Re-orient the matrix
			OrthoNormalize(myUp);

			// Rotate the matrix
			RotateAboutLocalY(yRot);
			RotateAboutLocalX(xRot);
		}
		else if (!IsZero(a))
		{
			LookAt(point, myUp);
		}
		else
		{
			// Re-orient the matrix
			OrthoNormalize(myUp);
		}

		return *this;
	}
	const mat4&			mat4::OrthoNormalize(const vec3& up /*= vec3(0, 1, 0)*/)
	{
		xAxis = vec4(up, 0).Cross(zAxis.SetNormalized());
		if (xAxis.IsZeroVector())
		{
			xAxis = vec4(1, 0, 0, 0).Cross(zAxis.Normalized());
		}
		yAxis = zAxis.Cross(xAxis.SetNormalized());
		return *this;
	}

	const mat4&			mat4::InvertAxisX()
	{
		mat4 identity;
		identity.xAxis.x = -1;

		*this *= identity;
		return *this;
	}
	const mat4&			mat4::InvertAxisY()
	{
		mat4 identity;
		identity.yAxis.y = -1;

		*this *= identity;
		return *this;
	}
	const mat4&			mat4::InvertAxisZ()
	{
		mat4 identity;
		identity.zAxis.z = -1;

		*this *= identity;
		return *this;
	}

	/***** Init *****/
	/*static*/ mat4		mat4::ZeroMatrix()
	{
		static const mat4 zeroMat(
			0, 0, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 0
			);
		return zeroMat;
	}
	/*static*/ mat4		mat4::IdentityMatrix()
	{
		static const mat4 identity(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
			);
		return identity;
	}
	const mat4&			mat4::SetIdentityMatrix()
	{
		*this = mat4();
		return *this;
	}
	const mat4&			mat4::SetZeroMatrix()
	{
		*this = ZeroMatrix();
		return *this;
	}

	mat4::mat4() :
		e11(1), e12(0), e13(0), e14(0),
		e21(0), e22(1), e23(0), e24(0),
		e31(0), e32(0), e33(1), e34(0),
		e41(0), e42(0), e43(0), e44(1)
	{

	}
	mat4::mat4(const real m11, const real m12, const real m13, const real m14,
		const real m21, const real m22, const real m23, const real m24,
		const real m31, const real m32, const real m33, const real m34,
		const real m41, const real m42, const real m43, const real m44) :
		e11(m11), e12(m12), e13(m13), e14(m14),
		e21(m21), e22(m22), e23(m23), e24(m24),
		e31(m31), e32(m32), e33(m33), e34(m34),
		e41(m41), e42(m42), e43(m43), e44(m44)
	{

	}
	mat4::mat4(const vec4& x, const vec4& y, const vec4& z, const vec4& w) :
		mat4(x.x, x.y, x.z, x.w,
		y.x, y.y, y.z, y.w,
		z.x, z.y, z.z, z.w,
		w.x, w.y, w.z, w.w)
	{

	}
	mat4::mat4(const quat& q)
	{
		*this = q.ToRotationMatrix4D();
	}
	mat4::mat4(const quat& q, const vec3& translation)
	{
		*this = q.ToRotationMatrix4D();
		wAxis = vec4(translation, 1);
	}
	mat4::mat4(const real* arr)
		:mat4(arr[0], arr[1], arr[2], arr[3],
		arr[4], arr[5], arr[6], arr[7],
		arr[8], arr[9], arr[10], arr[11],
		arr[12], arr[13], arr[14], arr[15])
	{

	}
	mat4& mat4::operator=(const real* arr)
	{
		for (int i = 0; i < NUM_ELEMENTS; ++i)
		{
			e[i] = arr[i];
		}
		return *this;
	}

	mat3 mat4::To3x3() const
	{
		return mat3(e11, e12, e13,
			e21, e22, e23,
			e31, e32, e33);
	}
	quat mat4::ToQuaternion() const
	{
		real x, y, z, w;

		// Determine which component has the largest abs.
		real fourWSquaredMinus1 = e11 + e22 + e33;
		real fourXSquaredMinus1 = e11 - e22 - e33;
		real fourYSquaredMinus1 = e22 - e11 - e33;
		real fourZSquaredMinus1 = e33 - e11 - e22;

		int biggestIndex = 0;
		real fourBiggestSquaredMinus1 = fourWSquaredMinus1;
		if (fourXSquaredMinus1 > fourBiggestSquaredMinus1)
		{
			fourBiggestSquaredMinus1 = fourXSquaredMinus1;
			biggestIndex = 1;
		}
		if (fourYSquaredMinus1 > fourBiggestSquaredMinus1)
		{
			fourBiggestSquaredMinus1 = fourYSquaredMinus1;
			biggestIndex = 2;
		}
		if (fourZSquaredMinus1 > fourBiggestSquaredMinus1)
		{
			fourBiggestSquaredMinus1 = fourZSquaredMinus1;
			biggestIndex = 3;
		}

		// Perform square root and division
		real biggestVal = sqrt(fourBiggestSquaredMinus1 + 1) * real(0.5);
		real mult = real(0.25) / biggestVal;

		// Apply table to compute quaternion values
		switch (biggestIndex)
		{
		case 0: w = biggestVal;
			x = (e23 - e32)* mult;
			y = (e31 - e13)* mult;
			z = (e12 - e21)* mult;
			break;
		case 1:	x = biggestVal;
			w = (e23 - e32) * mult;
			y = (e12 + e21) * mult;
			z = (e31 + e13) * mult;
			break;
		case 2:	y = biggestVal;
			w = (e31 - e13)* mult;
			x = (e12 + e21)* mult;
			z = (e23 + e32)* mult;
			break;
		case 3:	z = biggestVal;
			w = (e12 - e21)* mult;
			x = (e31 + e13)* mult;
			y = (e23 + e32)* mult;
			break;
		}

		return quat(w, x, y, z);
	}
	std::pair<quat, vec3> mat4::ToQuaternionPosition() const
	{
		quat q = ToQuaternion();
		vec3 pos = vec3(wAxis[0], wAxis[1], wAxis[2]);
		return std::pair<quat, vec3>(q, pos);
	}
	vec3 mat4::ToAngles() const
	{
		vec3 ret;

		real sp = -e32;
		if (sp <= -1)
			ret.x = -PI / 2;
		else if (sp >= 1)
			ret.x = PI / 2;
		else
			ret.x = asin(sp);

		if (abs(sp) > 1 - EPSILON)
		{
			ret.y = atan2(-e13, e11);
			ret.z = 0;
		}
		else
		{
			ret.y = atan2(e31, e33);
			ret.z = atan2(e12, e22);
		}
		return ret;
	}
}