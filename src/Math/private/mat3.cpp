#include "mat3.h"
#include "mat4.h"
#include "quat.h"
#include "vec2.h"
#include "tmat.hpp"

namespace gas
{
	typedef tmat<mat3, vec3> Algorithm;

	/***** Public Interface *****/
	// Accessors
	real&			mat3::operator[](const int i)
	{
		return e[i];
	}
	const real&		mat3::operator[](const int i) const
	{
		return e[i];
	}
	mat4			mat3::ToRotation3D() const
	{
		// TODO:
		return mat4();
	}

	vec3			mat3::Column(const int i) const
	{
		return vec3(e[i + 0 * NUM_DIMENSIONS], e[i + 1 * NUM_DIMENSIONS], e[i + 2 * NUM_DIMENSIONS]);
	}
	vec3			mat3::ColumnX() const
	{
		return vec3(e11, e21, e31);
	}
	vec3			mat3::ColumnY() const
	{
		return vec3(e12, e22, e32);
	}
	vec3			mat3::ColumnZ() const
	{
		return vec3(e13, e23, e33);
	}
	void			mat3::SetColumnX(const vec3& src)
	{
		e11 = src.x;
		e21 = src.y;
		e31 = src.z;
	}
	void			mat3::SetColumnY(const vec3& src)
	{
		e12 = src.x;
		e22 = src.y;
		e32 = src.z;
	}
	void			mat3::SetColumnZ(const vec3& src)
	{
		e13 = src.x;
		e23 = src.y;
		e33 = src.z;
	}

	// Comparisons
	bool			mat3::operator==(const mat3& rhs) const
	{
		return Algorithm::IsMatrixEqual(*this, rhs);
	}
	bool			mat3::operator!=(const mat3& rhs) const
	{
		return !(*this == rhs);
	}
	bool			mat3::IsZeroMatrix() const
	{
		return Algorithm::IsZeroMatrix(*this);
	}

	// Arithmetic
	/*friend*/ mat3 operator+(const mat3& m)
	{
		return m;
	}
	mat3			mat3::operator+(const mat3& rhs) const
	{
		return Algorithm::Add(*this, rhs);
	}
	const mat3&		mat3::operator+=(const mat3& rhs)
	{
		*this = *this + rhs;
		return *this;
	}

	/*friend*/ mat3 operator-(const mat3& m)
	{
		return Algorithm::Negative(m);
	}
	mat3			mat3::operator-(const mat3& rhs) const
	{
		return Algorithm::Subtract(*this, rhs);
	}
	const mat3&		mat3::operator-=(const mat3& rhs)
	{
		*this = *this - rhs;
		return *this;
	}

	mat3			mat3::operator*(const mat3& rhs) const
	{
		return Algorithm::Multiply(*this, rhs);
	}
	const mat3&		mat3::operator*=(const mat3& rhs)
	{
		*this = *this * rhs;
		return *this;
	}
	vec3			mat3::operator*(const vec3& v) const
	{
		return Algorithm::Transform(*this, v);
	}
	mat3			mat3::operator*(const real s) const
	{
		return Algorithm::Multiply(*this, s);
	}
	const mat3&		mat3::operator*=(const real s)
	{
		*this = *this * s;
		return *this;
	}
	/*friend*/ mat3 operator*(const real s, const mat3& m)
	{
		return m * s;
	}

	mat3			mat3::operator/(const real s) const
	{
		return Algorithm::Divide(*this, s);
	}
	const mat3&		mat3::operator/=(const real s)
	{
		*this = *this / s;
		return *this;
	}

	// Operations
	const mat3&		mat3::Transpose()
	{
		Algorithm::Transpose(*this);
		return *this;
	}
	real			mat3::Determinant() const
	{
		real minor11 = ((e22*e33) - (e32*e23));
		real minor12 = ((e21*e33) - (e31*e23));
		real minor13 = ((e21*e32) - (e31*e22));

		real cofactor11 = minor11;
		real cofactor12 = -(minor12);
		real cofactor13 = minor13;

		real det1 = cofactor11*e11;
		real det2 = cofactor12*e12;
		real det3 = cofactor13*e13;

		real det = (IsZero(det1 + det2 + det3) ? 0 : det1 + det2 + det3);
		return det;
	}
	mat3			mat3::Inverse() const
	{
		mat3 result;
		const real det = Determinant();
		if (!IsZero(det))
		{
			const real invDet = 1 / det;

			result.e11 = (e22*e33 - e23*e32) * invDet;
			result.e12 = (e13*e32 - e12*e33) * invDet;
			result.e13 = (e12*e23 - e13*e22) * invDet;
			result.e21 = (e23*e31 - e21*e33) * invDet;
			result.e22 = (e11*e33 - e13*e31) * invDet;
			result.e23 = (e13*e21 - e11*e23) * invDet;
			result.e31 = (e21*e32 - e22*e31) * invDet;
			result.e32 = (e12*e31 - e11*e32) * invDet;
			result.e33 = (e11*e22 - e12*e21) * invDet;
		}

		return result;
	}
	const mat3&		mat3::Invert()
	{
		*this = Inverse();
		return *this;
	}

	// Transformations
	const mat3&			mat3::TransformWorld(const mat3& transformMat)
	{
		*this = Algorithm::TransformWorld(*this, transformMat);
		return *this;
	}
	const mat3&			mat3::TransformLocal(const mat3& transformMat)
	{
		*this = Algorithm::TransformLocal(*this, transformMat);
		return *this;
	}
	/*static*/ mat3		mat3::CreateXRotationMatrix(const real radians)
	{
		return mat3(
			1, 0, 0,
			0, cos(radians), sin(radians),
			0, -(sin(radians)), cos(radians)
			);
	}
	/*static*/ mat3		mat3::CreateYRotationMatrix(const real radians)
	{
		return mat3(
			cos(radians), 0, -(sin(radians)),
			0, 1, 0,
			sin(radians), 0, cos(radians)
			);
	}
	/*static*/ mat3		mat3::CreateZRotationMatrix(const real radians)
	{
		return mat3(
			cos(radians), sin(radians), 0,
			-(sin(radians)), cos(radians), 0,
			0, 0, 1
			);
	}
	const mat3&			mat3::RotateAboutLocalX(const real xRadians)
	{
		mat3 rotMatrix = CreateXRotationMatrix(xRadians);
		return TransformLocal(rotMatrix);
	}
	const mat3&			mat3::RotateAboutLocalY(const real yRadians)
	{
		mat3 rotMatrix = CreateYRotationMatrix(yRadians);
		return TransformLocal(rotMatrix);
	}
	const mat3&			mat3::RotateAboutLocalZ(const real zRadians)
	{
		mat3 rotMatrix = CreateZRotationMatrix(zRadians);
		return TransformLocal(rotMatrix);
	}
	const mat3&			mat3::RotateAboutWorldX(const real xRadians)
	{
		mat3 rotMatrix = CreateXRotationMatrix(xRadians);
		return TransformWorld(rotMatrix);
	}
	const mat3&			mat3::RotateAboutWorldY(const real yRadians)
	{
		mat3 rotMatrix = CreateYRotationMatrix(yRadians);
		return TransformWorld(rotMatrix);
	}
	const mat3&			mat3::RotateAboutWorldZ(const real zRadians)
	{
		mat3 rotMatrix = CreateZRotationMatrix(zRadians);
		return TransformWorld(rotMatrix);
	}

	// Behaviors
	const mat3&			mat3::OrthoNormalize(const vec3& up)
	{
		xAxis = up.Cross(zAxis.SetNormalized());
		if (xAxis.IsZeroVector())
		{
			xAxis = vec3(1, 0, 0).Cross(zAxis.Normalized());
		}
		yAxis = zAxis.Cross(xAxis.SetNormalized());
		return *this;
	}

	// Handedness
	const mat3&			mat3::InvertAxisX()
	{
		mat3 identity;
		identity.xAxis.x = -1;

		*this *= identity;
		return *this;
	}
	const mat3&			mat3::InvertAxisY()
	{
		mat3 identity;
		identity.yAxis.y = -1;

		*this *= identity;
		return *this;
	}
	const mat3&			mat3::InvertAxisZ()
	{
		mat3 identity;
		identity.zAxis.z = -1;

		*this *= identity;
		return *this;
	}

	/*static*/ mat3		mat3::ZeroMatrix()
	{
		static const mat3 zeroMat(
			0, 0, 0,
			0, 0, 0,
			0, 0, 0
			);
		return zeroMat;
	}
	const mat3&			mat3::SetZeroMatrix()
	{
		*this = ZeroMatrix();
		return *this;
	}
	/*static*/ mat3		mat3::IdentityMatrix()
	{
		static const mat3 identity(
			1, 0, 0,
			0, 1, 0,
			0, 0, 1
			);
		return identity;
	}
	const mat3&			mat3::SetIdentityMatrix()
	{
		*this = mat3();
		return *this;
	}

	mat3::mat3() :
		e11(1), e12(0), e13(0),
		e21(0), e22(1), e23(0),
		e31(0), e32(0), e33(1)
	{

	}
	mat3::mat3(const real m11, const real m12, const real m13,
		const real m21, const real m22, const real m23,
		const real m31, const real m32, const real m33) :
		e11(m11), e12(m12), e13(m13),
		e21(m21), e22(m22), e23(m23),
		e31(m31), e32(m32), e33(m33)
	{

	}
	mat3::mat3(const vec3& x, const vec3& y, const vec3& z) :
		mat3(
		x.x, x.y, x.z,
		y.x, y.y, y.z,
		z.x, z.y, z.z)
	{

	}
	mat3::mat3(const quat& q)
	{
		*this = q.ToRotationMatrix3D();
	}
	mat3::mat3(const real* arr) :
		mat3(
		arr[0], arr[1], arr[2],
		arr[3], arr[4], arr[5],
		arr[6], arr[7], arr[8])
	{

	}
	mat3& mat3::operator=(const real* arr)
	{
		for (int i = 0; i < NUM_ELEMENTS; ++i)
		{
			e[i] = arr[i];
		}
		return *this;
	}

	mat4 mat3::To4x4() const
	{
		return mat4(e11,e12,e13,0,
					e21,e22,e23,0,
					e31,e32,e33,0,
					0,0,0,1);
	}
	quat mat3::ToQuaternion() const
	{
		return To4x4().ToQuaternion();
	}
	vec3 mat3::ToAngles() const
	{
		vec3 ret;

		real sp = -e32;
		if (sp <= -1)
			ret.x = -PI / 2;
		else if (sp >= 1)
			ret.x = PI / 2;
		else
			ret.x = asin(sp);

		if (abs(sp) > 1 - EPSILON)
		{
			ret.y = atan2(-e13, e11);
			ret.z = 0;
		}
		else
		{
			ret.y = atan2(e31, e33);
			ret.z = atan2(e12, e22);
		}
		return ret;
	}
}