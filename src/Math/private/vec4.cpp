#include "vec4.h"
#include "vec2.h"
#include "vec3.h"
#include "mat4.h"
#include "tvec.hpp"

namespace gas
{
	/***** Aliases *****/
	typedef tvec<vec4, mat4> Algorithm;

	/***** Public Interface *****/
	/*static*/ vec4		vec4::WorldUp()
	{
		static const vec4 up(0, 1, 0, 0);
		return up;
	}

	// Accessors
	real&				vec4::operator[](const int i)
	{
		return e[i];
	}
	const real&			vec4::operator[](const int i) const
	{
		return e[i];
	}
	vec3				vec4::AsVec3() const
	{
		return vec3(x, y, z);
	}

	// Comparisons
	bool				vec4::IsEqualTo(const vec4& rhs) const
	{
		return Algorithm::IsEqualVector(*this, rhs);
	}
	bool				vec4::operator==(const vec4& rhs) const
	{
		return Algorithm::IsEqualVector(*this, rhs);
	}
	bool				vec4::operator!=(const vec4& rhs) const
	{
		return !Algorithm::IsEqualVector(*this, rhs);
	}
	bool				vec4::IsZeroVector() const
	{
		return Algorithm::IsZeroVector(*this);
	}

	// Arithmetic
	real				vec4::SumComponents() const
	{
		return Algorithm::SumComponents(*this);
	}
	/*friend*/ const vec4& operator+(const vec4& v)
	{
		return v;
	}
	vec4				vec4::operator+(const vec4& rhs) const
	{
		return Algorithm::Add(*this, rhs);
	}
	const vec4&			vec4::operator+=(const vec4& rhs)
	{
		*this = *this + rhs;
		return *this;
	}

	/*friend*/ vec4		operator-(const vec4& v)
	{
		return Algorithm::Negative(v);
	}
	vec4				vec4::operator-(const vec4& rhs) const
	{
		return Algorithm::Subtract(*this, rhs);
	}
	const vec4&			vec4::operator-=(const vec4& rhs)
	{
		*this = *this - rhs;
		return *this;
	}

	vec4				vec4::operator*(const real s) const
	{
		return Algorithm::Multiply(*this, s);
	}
	const vec4&			vec4::operator*=(const real s)
	{
		*this = *this * s;
		return *this;
	}
	/*friend*/ vec4		operator*(const real s, const vec4& v)
	{
		return v * s;
	}

	vec4				vec4::operator/(const real s) const
	{
		return Algorithm::Divide(*this, s);
	}
	const vec4&			vec4::operator/=(const real s)
	{
		*this = *this / s;
		return *this;
	}

	// Operations
	vec4				vec4::Normalized() const
	{
		return Algorithm::Normalized(*this);
	}
	const vec4&			vec4::SetNormalized()
	{
		*this = Normalized();
		return *this;
	}
	real				vec4::Magnitude() const
	{
		return Algorithm::Magnitude(*this);
	}
	real				vec4::MagnitudeSquared() const
	{
		return Algorithm::MagnitudeSquared(*this);
	}
	real				vec4::Dot(const vec4& rhs) const
	{
		return Algorithm::Dot(*this, rhs);
	}
	real				vec4::ComponentOnto(const vec4& rhs) const
	{
		return Algorithm::ComponentOnto(*this, rhs);
	}
	vec4				vec4::ProjectionOnto(const vec4& rhs) const
	{
		return Algorithm::ProjectionOnto(*this, rhs);
	}
	vec4				vec4::Cross(const vec4& rhs) const
	{
		vec4 crossed;
		crossed.x = (y * rhs.z - rhs.y * z);
		crossed.y = -(x * rhs.z - rhs.x * z);
		crossed.z = (x * rhs.y - rhs.x * y);
		crossed.w = 0;
		return crossed;
	}
	vec4&				vec4::Transform(const mat4& m)
	{
		*this = m * (*this);
		return *this;
	}

	/***** Init *****/
	/*static*/ vec4			vec4::ZeroVector()
	{
		return vec4(0, 0, 0, 0);
	}

	vec4::vec4(const vec2& rhs, const real _z, const real _w) :
		vec4(rhs.x, rhs.y, _z, _w)
	{

	}
	vec4::vec4(const vec3& rhs, const real _w) :
		vec4(rhs.x, rhs.y, rhs.z, _w)
	{

	}
	vec4::vec4(const real _x, const real _y, const real _z, const real _w) :
		x(_x), y(_y), z(_z), w(_w)
	{

	}
	/*explicit*/ vec4::vec4(const real* arr) :
		vec4(arr[0], arr[1], arr[2], arr[3])
	{

	}
	vec4& vec4::operator=(const real* arr)
	{
		x = arr[0];
		y = arr[1];
		z = arr[2];
		w = arr[3];
		return *this;
	}
}