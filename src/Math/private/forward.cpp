#include "forward.h"
#include <cmath>

namespace gas
{
	GOLDEN_API real AsDegrees(const real radians)
	{
		return radians * 180 / PI;
	}

	GOLDEN_API real AsRadians(const real degrees)
	{
		return degrees * PI / 180;
	}

	GOLDEN_API bool IsZero(const real n)
	{
		return ((n > 0 ? n : -n) <= EPSILON);
	}

	GOLDEN_API bool IsEqual(const real a, const real b)
	{
		if (a > b)
			return abs(a - b) < EPSILON;
		else
			return abs(b - a) < EPSILON;
	}

}