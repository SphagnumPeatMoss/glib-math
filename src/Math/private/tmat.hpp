#pragma once
#include "forward.h"
#include <cmath>

namespace gas
{
	template <typename mat, typename vec> struct tmat
	{
	public:
		// Comparisons
		static bool		IsMatrixEqual(const mat& a, const mat& b)
		{
			for (int i = 0; i < mat::NUM_ELEMENTS; ++i)
			{
				if (!IsEqual(a[i], b[i]))
					return false;
			}
			return true;
		}
		static bool		IsZeroMatrix(const mat& m)
		{
			for (int i = 0; i < mat::NUM_ELEMENTS; ++i)
			{
				if (!IsZero(m[i]))
					return false;
			}
			return true;
		}

		// Arithmetic
		static mat		Negative(const mat& m)
		{
			mat result;
			for (int i = 0; m.NUM_ELEMENTS; ++i)
			{
				result[i] = -m[i];
			}
			return result;
		}
		static mat		Add(const mat& a, const mat& b)
		{
			mat m;
			for (int i = 0; i < mat::NUM_ELEMENTS; ++i)
			{
				m[i] = a[i] + b[i];
			}
			return m;
		}
		static mat		Subtract(const mat& a, const mat& b)
		{
			mat diff;
			for (int i = 0; i < mat::NUM_ELEMENTS; ++i)
			{
				diff[i] = a[i] - b[i];
			}
			return diff;
		}
		static mat		Multiply(const mat& M, const mat& N)
		{
			mat result;
			vec A, B;

			for (int a = 0; a < mat::NUM_ELEMENTS;) {				// Go to each element of result matrix
				for (int b = 0; b < mat::NUM_DIMENSIONS; ++b) {		//    Go to each row of M
					for (int c = 0; c < vec::NUM_ELEMENTS; ++c)		//		 Go to each element in row
						A[c] = M[mat::NUM_DIMENSIONS*b + c];		//			Build vec A from the row in mat M

					for (int c = 0; c < mat::NUM_DIMENSIONS; ++c) {	//		 Go to each column in matrix N
						for (int d = 0; d < mat::NUM_DIMENSIONS; ++d)//			Go to each element in column
							B[d] = N[c + mat::NUM_DIMENSIONS * d];	//			   Build vec B from the column in mat N

						result[a++] = A.Dot(B);						//	Fill in matrix result with A Dot B
					}
				}
			}

			return result;
		}
		static mat		Multiply(const mat& m, const real s)
		{
			mat result;
			for (int i = 0; i < mat::NUM_ELEMENTS; ++i)
			{
				result[i] = m[i] * s;
			}
			return result;
		}
		static mat		Divide(const mat& m, const real s)
		{
			assert(!IsZero(s) && "Attempt to divide by zero.");
			mat result;
			for (int i = 0; i < mat::NUM_ELEMENTS; ++i)
			{
				result[i] = m[i] / s;
			}
			return result;
		}

		// Operations
		static void		Transpose(mat& m)
		{
			mat result;
			for (int i = 0; i < mat::NUM_DIMENSIONS; ++i)
			{
				result.axis[i] = m.Column(i);
			}
			m = result;
		}

		// Transformations
		static vec		Transform(const mat& m, const vec& v)
		{
			vec result;
			for (int i = 0; i < vec::NUM_ELEMENTS; ++i)
			{
				result[i] = v.Dot(m.Column(i));
			}
			return result;
		}
		static mat		TransformWorld(const mat& m, const mat& transform)
		{
			return m * transform;
		}
		static mat		TransformLocal(const mat& m, const mat& transform)
		{
			return transform * m;
		}
	};
}